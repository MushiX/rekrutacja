# Koncepcja na wykonanie aplikacji
W języku DDL utworzę schemat bazy danych zawierający dwie tabele:

**Produkty**

| id | cena | stan_magazynowy |
|----|------|-----------------|
|  1 |      |                 |
|  2 |      |                 |
|  3 |      |                 |

**ProduktyTłumaczenia**

| produkt_id | język | nazwa_produktu | opis |
|------------|-------|----------------|------|
|          1 | pl_PL |          masło |      |
|          1 | en_US |         butter |      |
|          3 | pl_PL |         ołówek |      |

W taki sposób można zrealizować problem posiadania tłumaczeń wpisów dostępnych w bazie danych.
Pole **id** tabeli **ProduktyTłumaczenia** jest kluczem obcym tabeli **Produkty**

## Użyte technologie
- PHP 7+
- biblioteka Sockets

Zastosowanie biblioteki sockets gwarantuje działanie aplikacji serwerowej bez installacji
dodatkowych bibliotek. Gniazda zostały pierwotnie stworzone na potrzeby systemu Unix
w języku C, których współtwórcą jest Dennis Ritchie. Stworzone sockety noszą nazwę
"Berkeley Sockets" i szybko doczekały się swojej implementacji na systemy Windows pod nazwą
"WinSock". Gniazda doczekały się przeportowania na większość języków programowania.

Użyłem gniazd w związku z ich stabilnością, długoletnią tradycją, a także dlatego że ich
implementacja znajduje się w każdym systemie operacyjnym.

## Uruchomienie projektu
**Przed przystąpieniem do uruchomienia serwera należy:**
- uruchomić serwer MySQL lub MariaDB (preferowana opcja)
- utworzyć odpowiednio przygotowaną bazę danych

**W tym celu *wewnątrz folderu z aplikacją* można użyć poleceń:**
```bash
$ systemctl start mariadb
$ mysql -u root < schema.sql
```

## PHPSockets
**Serwer uruchamia się poleceniem:**
```bash
$ php server.php
```

Można również uruchomić serwer, który będzie nasługiwał na innym adresie oraz innym porcie. Poniżej przykład
z wartościami domyślnymi:
```bash
$ php server.php localhost 8001
```