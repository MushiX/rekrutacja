CREATE DATABASE IF NOT EXISTS warehouse;
USE warehouse;

DROP TABLE IF EXISTS productTranslate;
DROP TABLE IF EXISTS product;

CREATE TABLE product(
 id         int unsigned    NOT NULL AUTO_INCREMENT,
 price      varchar(50)     NOT NULL,
 stock      varchar(255)    NOT NULL,
 PRIMARY KEY (id)
);

CREATE TABLE productTranslate(
 productId  int unsigned    NOT NULL,
 language   varchar(5)      NOT NULL,
 title      varchar(255)    NOT NULL,
 content    text,
 FOREIGN KEY (productId) REFERENCES product(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# Sample data
INSERT INTO product (id, price, stock) VALUES (1, 150, 50);
INSERT INTO productTranslate (productId, language, title, content)
    VALUES (1, "pl_PL", "Symfonia C++", "Książka o programowaniu w C++");
INSERT INTO productTranslate (productId, language, title, content)
    VALUES (1, "en_US", "Symfony C++", "Book about programing in C++ language");