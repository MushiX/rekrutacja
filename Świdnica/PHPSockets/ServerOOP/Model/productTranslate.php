<?php
require_once "Interface/crud.php";

class productTranslate implements crud
{
	private $table = "productTranslate";

	/**
	 * [0]=>SHOW, [1]=>productTranslate, [2]=>productId
	 * @param array $cmd
	 * @return String
	 */
	public function show(Array $cmd): String
	{
		return "SELECT * FROM {$this->table} WHERE productId={$cmd[2]}";
	}

	/**
	 * [0]=>ADD, [1]=>productTranslate, [2]=>productId, [3]=>language, [4]=>title, [5]=>content
	 * @param array $cmd
	 * @return String
	 */
	public function add(Array $cmd): ?String
	{
		if(is_numeric($cmd[2])){
			if(sizeof($cmd) == 6)
				return "INSERT INTO {$this->table} (productId, language, title, content) "
							."VALUES ({$cmd[2]}, \"{$cmd[3]}\", \"{$cmd[4]}\", \"{$cmd[5]}\")";
			elseif(sizeof($cmd) == 5)
				return "INSERT INTO {$this->table} (productId, language, title) "
							."VALUES ({$cmd[2]}, \"{$cmd[3]}\", \"{$cmd[4]}\")";
		}

		return null;
	}

	/**
	 * [0]=>EDIT, [1]=>productTranslate, [2]=>productId, [3]=>language, [4]=>title, ?[5]=>content
	 * @param array $cmd
	 * @return String
	 */
	public function edit(Array $cmd): ?String
	{
		if(is_numeric($cmd[2])){
			if(sizeof($cmd) == 6)
				return "UPDATE {$this->table} SET title=\"{$cmd[4]}\", content=\"{$cmd[5]}\" "
							."WHERE productId={$cmd[2]} AND language=\"{$cmd[3]}\"";
			elseif(sizeof($cmd) == 5)
				return "UPDATE {$this->table} SET title=\"{$cmd[4]}\" "
							."WHERE productId={$cmd[2]} AND language=\"{$cmd[3]}\"";
		}

		return null;
	}

	/**
	 * [0]=>DELETE, [1]=>productTranslate, [2]=>productId, ?[3]=>language
	 * @param array $cmd
	 * @return String
	 */
	public function delete(Array $cmd): ?String
	{
		if(sizeof($cmd) > 2 && is_numeric($cmd[2])){
			if(sizeof($cmd) == 4)
				return "DELETE FROM {$this->table} WHERE productId={$cmd[2]} AND language=\"{$cmd[3]}\"";
			else
				return "DELETE FROM {$this->table} WHERE productId={$cmd[2]}";
		}

		return null;
	}
}