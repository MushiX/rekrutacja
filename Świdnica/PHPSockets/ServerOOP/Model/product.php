<?php
require_once "Interface/crud.php";

class product implements crud
{
	private $table = "product";

	/**
	 * [0]=>SHOW, [1]=>product, [2]=>id
	 * @param array $cmd
	 * @return String
	 */
	public function show(Array $cmd): String
	{
		return "SELECT * FROM {$this->table} WHERE id={$cmd[2]}";
	}

	/**
	 * [0]=>ADD, [1]=>product, [2]=>price, [3]=>stock
	 * @param array $cmd
	 * @return String
	 */
	public function add(Array $cmd): ?String
	{
		if(sizeof($cmd) == 4 && is_numeric($cmd[2]) && is_numeric($cmd[3])){
			return "INSERT INTO {$this->table} (price, stock) VALUES ({$cmd[2]}, {$cmd[3]})";
		}

		return null;
	}

	/**
	 * [0]=>EDIT, [1]=>product, [2]=>id, [3]=>price, ?[4]=>stock
	 * @param array $cmd
	 * @return String
	 */
	public function edit(Array $cmd): ?String
	{
		if(is_numeric($cmd[2])){
			if(sizeof($cmd) == 5 && is_numeric($cmd[3]) && is_numeric($cmd[4]))
				return "UPDATE {$this->table} SET price={$cmd[3]}, stock={$cmd[4]} WHERE id={$cmd[2]}";
			elseif(sizeof($cmd) == 4 && is_numeric($cmd[3]))
				return "UPDATE {$this->table} SET price={$cmd[3]} WHERE id={$cmd[2]}";
		}

		return null;
	}

	/**
	 * [0]=>DELETE, [1]=>product, [2]=>id
	 * @param array $cmd
	 * @return String
	 */
	public function delete(Array $cmd): ?String
	{
		if(sizeof($cmd) == 3 && is_numeric($cmd[2]))
			return "DELETE FROM productTranslate WHERE productId={$cmd[2]};"
						."DELETE FROM {$this->table} WHERE id={$cmd[2]}";

		return null;
	}
}