<?php
require_once 'Helper/db.php';
require_once 'Helper/socket.php';

$VERSION = "v0.0.3";
$PASSWORD = "SECRET";

error_reporting(E_ERROR);
set_time_limit(0);
ob_implicit_flush();

$db = new Database(
	'mysql:host=localhost;dbname=warehouse;charset=utf8',
	'root',
	'',
	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
);

switch($argc){
	case 2: $config = [ "address"=>$argv[1], "port"=>8001 ];
		break;
	case 3:	$config = [ "address"=>$argv[1], "port"=>$argv[2] ];
		break;
	default: $config = [ "address"=>"localhost", "port"=>8001 ];
}

$sock = initSocket($config);

$clients = [];

do {
	$read = [];
	$read[] = $sock;

	$read = array_merge($read, $clients);

	if(socket_select($read,$write = NULL, $except = NULL, $tv_sec = 5) < 1)
		continue;

	if (in_array($sock, $read)) {
		if (($client = socket_accept($sock)) === false) {
			echo "Cannot socket accept: ".socket_strerror(socket_last_error($sock))."\n";
			break;
		}
		$clients[] = $client;
		$key = array_keys($clients, $client); //check this

		socket_write($client, "Send password to connect", 2048);
		if (false === ($cmd = socket_read($client, 2048))) {
			echo "Cannot socket read: " . socket_strerror(socket_last_error($client)) . "\n";
			break;
		}
		if($PASSWORD != $cmd)
			socket_close($client);

		socket_write($client, welcome($VERSION), 2048);
	}

	foreach($clients as $key => $client) {
		if (in_array($client, $read)) {
			if (false === ($cmd = socket_read($client, 2048))) {
				echo "Cannot socket read: " . socket_strerror(socket_last_error($client)) . "\n";
				break 2;
			}

			if (!$cmd = trim($cmd)) {
				if ($cmd == '') {
					echo "Client is dead\n";
					break;
				}
				continue;
			}

			switch ($cmd) {
				case (bool) preg_match('/^SHOW.*/i', $cmd):
					socket_write($client, $db->show(stringToArray($cmd)), 2048);
					break;

				case (bool) preg_match('/^ADD.*/i', $cmd):
					socket_write($client, $db->add(stringToArray($cmd)), 2048);
					break;

				case (bool) preg_match('/^EDIT.*/i', $cmd):
					socket_write($client, $db->edit(stringToArray($cmd)), 2048);
					break;

				case (bool) preg_match('/^DELETE.*/i', $cmd):
					socket_write($client, $db->delete(stringToArray($cmd)), 2048);
					break;

				case (bool) preg_match('/^exit.*/i', $cmd):
					unset($clients[$key]);
					socket_close($client);
					echo "Client exit\n";
					break 2;

				case (bool) preg_match('/^shutdown.*/i', $cmd):
					socket_close($client);
					break 3;

				default:
					$talkback = "server: '$cmd'\n";
					socket_write($client, $talkback, strlen($talkback));
					echo "client {$key}: $cmd\n";
			}
		}
	}

} while(true);

socket_close($sock);