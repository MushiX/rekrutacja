<?php
interface crud{
	public function show(Array $cmd): String;
	public function add(Array $cmd): ?String;
	public function edit(Array $cmd): ?String;
	public function delete(Array $cmd): ?String;
}