<?php

function welcome($version): string
{
	return sprintf("\nPHPSockets %s\n\n"
		."To Add Product type 'Add product [price] [stock]'\n"
		."To Add ProductTranslation type 'Add productTranslate [productId] [language] [title] [content]'\n"
		."To Show Product Info type 'Show product [id]'\n"
		."To Delete Product type 'DELETE product [id]'\n"
		."To quit, type 'exit'. To shut down the server type 'shutdown'.\n\n",
		$version
	);
}

function initSocket($config)
{
	if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
		echo "Cannot socket create: ".socket_strerror(socket_last_error())."\n";
		exit;
	}

	if (socket_bind($sock, $config["address"], $config["port"]) === false) {
		echo "Cannot socket bind: ".socket_strerror(socket_last_error($sock))."\n";
		exit;
	}

	if (socket_listen($sock, 5) === false) {
		echo "Cannot socket listen: ".socket_strerror(socket_last_error($sock))."\n";
		exit;
	}

	return $sock;
}

function stringToArray($text){
	return preg_split(
		"/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/",
		$text,
		0,
		PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
	);
}