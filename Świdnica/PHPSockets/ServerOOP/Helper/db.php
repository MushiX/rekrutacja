<?php
require_once "Interface/crud.php";
require_once "Model/product.php";
require_once "Model/productTranslate.php";

class Database implements crud
{
	private $tables = ['product', 'productTranslate'];
	private $db;
	private $product;
	private $productTranslate;

	public function __construct(string $dns, string $user, string $password, $options = [])
	{
		try {
			$this->db = new pdo($dns, $user, $password, $options);
		} catch (PDOException $ex) {
			die("Cannot connect to the database\n");
		}

		$this->product = new product();
		$this->productTranslate = new productTranslate();
	}

	public function show(Array $cmd): String
	{
		if(sizeof($cmd)==3 && $this->existTable($cmd[1]) && is_numeric($cmd[2])){
			$query = '';
			if($cmd[1]=='product'){
				$query = $this->db->query($this->product->show($cmd)) or die("Something wrong with database\n");
			}
			elseif($cmd[1]=='productTranslate'){
				$query = $this->db->query($this->productTranslate->show($cmd)) or die("Something wrong with database\n");
			}

			$data = [];
			while($row = $query->fetch(PDO::FETCH_ASSOC)){
				$data[] = $row;
			}

			return json_encode($data)."\n";
		}

		return "Given table '{$cmd[1]}' not exist\n";
	}

	public function add(Array $cmd): String
	{
		if($this->existTable($cmd[1])){
			if($cmd[1]=='product'){
				if(!$sql = $this->product->add($cmd))
					return "Syntax error! Use: ADD product [price] [stock]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
			}
			elseif($cmd[1]=='productTranslate'){
				if(!$sql = $this->productTranslate->add($cmd))
					return "Syntax error! Use: ADD productTranslate [productId] [language] [title] [content]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
				return "Success! Your item have productId: {$cmd[2]}, and language: {$cmd[3]}\n";
			}

			return "Success your item have id: ".$this->db->lastInsertId()."\n";
		}

		return "Given table '{$cmd[1]}' not exist\n";
	}

	public function edit(Array $cmd): String
	{
		if($this->existTable($cmd[1])){
			if($cmd[1]=='product'){
				if(!$sql = $this->product->edit($cmd))
					return "Syntax error! Use: EDIT product [id] [price] [stock]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
			}
			elseif($cmd[1]=='productTranslate'){
				if(!$sql = $this->productTranslate->edit($cmd))
					return "Syntax error! Use: EDIT productTranslate [productId] [language] [title] [content]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
			}

			return "Success your item has been modified\n";
		}

		return "Given table '{$cmd[1]}' not exist\n";
	}

	public function delete(Array $cmd): String
	{
		if($this->existTable($cmd[1])){
			if($cmd[1]=='product'){
				if(!$sql = $this->product->delete($cmd))
					return "Syntax error! Use: DELETE product [id]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
			}
			elseif($cmd[1]=='productTranslate'){
				if(!$sql = $this->productTranslate->delete($cmd))
					return "Syntax error! Use: DELETE productTranslate [productId] [language]\n";
				$this->db->query($sql) or die("Something wrong with database\n");
			}

			return "Successfully remove element\n";
		}

		return "Given table '{$cmd[1]}' not exist\n";
	}

	private function existTable(string $name): bool
	{
		return (array_search($name, $this->tables) === false) ? false : true;
	}
}