<?php
error_reporting(E_ALL);

switch($argc){
	case 2: $config = [ "address"=>gethostbyname($argv[1]), "port"=>8001 ];
		break;
	case 3:	$config = [ "address"=>gethostbyname($argv[1]), "port"=>$argv[2] ];
		break;
	default: $config = [ "address"=>gethostbyname("localhost"), "port"=>8001 ];
}

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if ($socket === false) {
	echo "Cannot socket create: ".socket_strerror(socket_last_error())."\n";
	exit;
}

echo "Attempting to connect to '".$config["address"]."' on port '".$config["port"]."'...\n\n";
$result = socket_connect($socket, $config["address"], $config["port"]);
if ($result === false) {
	echo "Cannot socket connect: ".socket_strerror(socket_last_error($socket))."\n";
	exit;
}


do{
	echo socket_read($socket, 2048);
	do{
		$cmd = readline(">> ");
	} while(empty($cmd));
	socket_write($socket, $cmd, strlen($cmd));
} while(!preg_match('/^exit.*|^shutdown.*/', $cmd));


echo "Closing socket...\n";
socket_close($socket);