<?php
$VERSION = "v0.0.2";
$PASSWORD = "SECRET";

try{
	$db = new pdo( 'mysql:host=localhost;dbname=warehouse;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch(PDOException $ex){
	die("Cannot connect to the database\n");
}

error_reporting(E_ERROR);
set_time_limit(0);
ob_implicit_flush();

switch($argc){
	case 2: $config = [ "address"=>$argv[1], "port"=>8001 ];
					break;
	case 3:	$config = [ "address"=>$argv[1], "port"=>$argv[2] ];
					break;
	default: $config = [ "address"=>"localhost", "port"=>8001 ];
}


if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
	echo "Cannot socket create: ".socket_strerror(socket_last_error())."\n";
	exit;
}

if (socket_bind($sock, $config["address"], $config["port"]) === false) {
	echo "Cannot socket bind: ".socket_strerror(socket_last_error($sock))."\n";
	exit;
}

if (socket_listen($sock, 5) === false) {
	echo "Cannot socket listen: ".socket_strerror(socket_last_error($sock))."\n";
	exit;
}

$clients = [];

do {
	$read = [];
	$read[] = $sock;

	$read = array_merge($read,$clients);

	if(socket_select($read,$write = NULL, $except = NULL, $tv_sec = 5) < 1)
	{
		continue;
	}

	if (in_array($sock, $read)) {
		if (($client = socket_accept($sock)) === false) {
			echo "Cannot socket accept: ".socket_strerror(socket_last_error($sock))."\n";
			break;
		}
		$clients[] = $client;
		$key = array_keys($clients, $client);


		socket_write($client, "Send password to connect", 2048);
		if (false === ($cmd = socket_read($client, 2048))) {
			echo "Cannot socket read: " . socket_strerror(socket_last_error($client)) . "\n";
			break;
		}
		if($PASSWORD != $cmd)
			socket_close($client);

		$msg = sprintf("\nPHPSockets %s\n\n"
			."To Add Product type 'Add product [price] [stock]'\n"
			."To Add ProductTranslation type 'Add productTranslate [productId] [language] [title] [content]'\n"
			."To Show Product Info type 'Show product [id]'\n"
			."To Delete Product type 'DELETE product [id]'\n"
			."To quit, type 'exit'. To shut down the server type 'shutdown'.\n\n",
			$VERSION
		);
		socket_write($client, $msg, strlen($msg));
	}

	foreach($clients as $key => $client) {
		if (in_array($client, $read)) {
			if (false === ($cmd = socket_read($client, 2048))) {
				echo "Cannot socket read: " . socket_strerror(socket_last_error($client)) . "\n";
				break 2;
			}

			if (!$cmd = trim($cmd)) {
				if ($cmd == '') {
					echo "Client is dead\n";
					break;
				}
				continue;
			}

			switch ($cmd) {
				case (bool) preg_match('/^SHOW.*/i', $cmd):
					$cmd = explode(' ', $cmd);
					if(sizeof($cmd) == 3 && existTable($cmd[1]) && is_numeric($cmd[2])){
						$column = getPrimaryKey($cmd[1]);
						$query = $db->query("SELECT * FROM {$cmd[1]} WHERE {$column}={$cmd[2]}")
						or die("Something wrong with database\n");

						$data = [];
						while($row = $query->fetch(PDO::FETCH_ASSOC)){
							$data[] = $row;
						}

						socket_write($client, json_encode($data)."\n", 2048);
					}
					elseif(!existTable($cmd[1])){
						socket_write($client, "Given table '{$cmd[1]}' not exist\n", 2048);
					}
					elseif(!is_numeric($cmd[2])){
						socket_write($client, "Wrong parameter '{$cmd[2]}', please use ID\n", 2048);
					}
					else{
						socket_write($client, "Syntax error! Use: SHOW [table_name] [id]\n", 2048);
					}
					break;

				case (bool) preg_match('/^ADD.*/i', $cmd):
					$cmd = preg_split(
						"/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/",
						$cmd,
						0,
						PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
					);

					if(!existTable($cmd[1])){
						socket_write($client, "Given table '{$cmd[1]}' not exist\n", 2048);
					}
					elseif(
									($cmd[1]=="product" && sizeof($cmd) == 4) ||
									($cmd[1]=="productTranslate" && sizeof($cmd) > 4 && sizeof($cmd) < 7)
								)
					{
						$sql = "INSERT INTO {$cmd[1]} ";
						if($cmd[1] == "product") $sql .= "(price, stock) ";
						if($cmd[1] == "productTranslate"){
							if(sizeof($cmd) == 6)
								$sql .= "(productId, language, title, content) ";
							else
								$sql .= "(productId, language, title) ";
						}
						$sql .= "VALUES (";
						for($i=2; $i<sizeof($cmd); $i++){
							$sql .= is_numeric($cmd[$i]) ? $cmd[$i] : '"'.$cmd[$i].'"';
							if($i+1 != sizeof($cmd)) $sql .= ", ";
						}
						$sql .= ");";

						$db->query($sql);

						socket_write($client, "Success your item have id: ".$db->lastInsertId()."\n", 2048);
					}
					else{
						if($cmd[1]=="product"){
							socket_write($client, "Syntax error! Use: ADD product [price] [stock]\n", 2048);
						}
						else{
							socket_write($client, "Syntax error! Use: ADD productTranslate [productId] [language] [title] [content]\n", 2048);
						}
					}
					break;

				case (bool) preg_match('/^EDIT.*/i', $cmd):
					$cmd = preg_split(
						"/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/",
						$cmd,
						0,
						PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
					);

					if(existTable($cmd[1]) && is_numeric($cmd[2])){
						$sql = "UPDATE {$cmd[1]} SET ";
						if($cmd[1]=="product"){
							$sql .= "price={$cmd[3]}, stock={$cmd[4]} ";
						}
						if($cmd[1]=="productTranslate"){
							$sql .= "title=\"{$cmd[4]}\", content=\"{$cmd[5]}\" WHERE productId={$cmd[2]} AND language=\"{$cmd[3]}\";";
						}
						$key = getPrimaryKey($cmd[1]);
						if($cmd[1]!='productTranslate') $sql .= "WHERE {$key}={$cmd[2]}";
						$db->query($sql);
						socket_write($client, "Success your item has been modified\n", 2048);
					}
					else{
						socket_write($client, "Syntax error! Use EDIT [table_name] [id] [col1_new_value] etc.\n", 2048);
					}

					break;

				case (bool) preg_match('/^DELETE.*/i', $cmd):
					$cmd = explode(' ', $cmd);
					if(sizeof($cmd)==3 && existTable($cmd[1]) && existTable($cmd[1])){
						$key = getPrimaryKey($cmd[2]);
						$db->query("DELETE FROM {$cmd[1]} WHERE {$key}={$cmd[2]}");
						socket_write($client, "Successfully remove element\n", 2048);
					}
					else{
						socket_write($client, "Syntax error! Use: DELETE [table_name] [id]'\n", 2048);
					}
					break;

				case (bool) preg_match('/^exit.*/', $cmd):
					unset($clients[$key]);
					socket_close($client);
					echo "Client exit\n";
					break 2;

				case (bool) preg_match('/^shutdown.*/', $cmd):
					socket_close($client);
					break 3;

				default:
					$talkback = "server: '$cmd'\n";
					socket_write($client, $talkback, strlen($talkback));
					echo "client {$key}: $cmd\n";
			}
		}
	}

} while (true);

socket_close($sock);

function existTable(string $name): bool
{
	$tables = ['product', 'productTranslate'];
	return (array_search($name, $tables) === false) ? false : true;
}

function getPrimaryKey(string $name): string
{
	switch($name){
		case "productTranslate":	return "productId";
	}
	return 'id';
}