<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<title>Zadanie 2</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
	<link rel="stylesheet" href="base.css"/>
</head>
<body>
	<nav class="navbar is-info">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <h1>MAGAZYN</h1>
      </a>
      <div class="navbar-burger burger" data-target="navigation">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div id="navigation" class="navbar-menu">
      <a class="navbar-item is-active" href="index.php">product.csv</a>
      <a class="navbar-item" href="import.php">wybrane produkty</a>

      <div class="navbar-end">
      	<a href="index.php" class="button">RESET</a>
      </div>
    </div>
  </nav>
  <div class="wrapper has-text-centered">
  	<form action="import.php" method="post">
  		<table class="table is-striped">
  			<thead>
  				<tr>
  				<?php
  					$lines = file('product.csv');

  					foreach ($lines as $line_num => $line){
  						$sign=substr($line, 0, strpos($line, ';')); $line=substr($line, strpos($line, ';')+1, -1);
  						$name=substr($line, 0, strpos($line, ';')); $line=substr($line, strpos($line, ';')+1, -1);
  						$active=substr($line, 0, strpos($line, ';')); $line=substr($line, strpos($line, ';')+1, -1);
  						$author=substr($line, 0, strpos($line, ';')); $line=substr($line, strpos($line, ';')+1, -1);
  						$publisher=substr($line, 0, strpos($line, ';')); $line=substr($line, strpos($line, ';')+1, -1);
  						$release=substr($line, 0, strpos($line, ';'));

  						if($line_num==0){ 
  							echo "<th>$sign</th><th>$name</th><th>$active</th><th>$author</th><th>$publisher</th><th>$release</th><th>Dodać?</th></tr></thead><tbody>"; 
  						} else {
  							echo "<tr><td>$sign</td><td>$name</td><td>$active</td><td>$author</td><td>$publisher</td><td>$release</td><td>".'<input type="checkbox" class="checkbox" name="item'.$line_num.'"/></td></tr>';
  						}
						}
						echo "</tbody></table>";
  				?>
  			</tbody>
  		</table>

		  <input type="submit" value="Zapisz" class="button is-info"/>
		</form>

  </div>
  <footer class="footer">
  	<p><a href="http://mushix.eu/cv" target="_blank">MushiX</a> &copy; wszelkie prawa zastrzeżone</p>
    <p id="clock"></p>
  </footer>

  <script src="base.js"></script>
  <script src="clock.js"></script>
</body>
</html>