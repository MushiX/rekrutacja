<?php
  $file = fopen('new.csv', 'w');
  $lines = file('product.csv');
  foreach ($lines as $line_num => $line){
    if($line_num==0 || !empty($_POST['item'.$line_num])){ 
      fwrite($file, $line);
    }
  }
  fclose($file);

  $file = 'new.csv';

  if (file_exists($file)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.basename($file).'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      readfile($file);
      exit;
  }
?>