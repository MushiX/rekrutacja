# Magazyn (Rails version)

### Instalation environment (Arch Linux)

```bash
$ sudo pacman -S ruby postgresql nodejs
$ sudo gem install rails --no-user-install
```

Inside the project:

```bash
$ cd Magazyn
$ bundle install
```

### How to run the project

```bash
$ rails s
```

