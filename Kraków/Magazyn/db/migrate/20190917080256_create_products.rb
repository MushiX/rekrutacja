class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.integer :symbol, unique: true
      t.string :name
      t.boolean :active
      t.integer :author_id
      t.integer :producer_id
      t.integer :publication_date_id

      t.timestamps
    end
  end
end
