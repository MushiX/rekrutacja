json.extract! product, :id, :symbol, :name, :active, :author_id, :producer_id, :publication_date_id, :created_at, :updated_at
json.url product_url(product, format: :json)
