# Magazyn (Rails version) _deprecated_

### Instalation environment (Arch Linux)

```bash
$ sudo pacman -S ruby postgresql nodejs
$ sudo gem install rails --no-user-install
```

Inside the project:

```bash
$ cd Magazyn
$ bundle install
```

### How to run the project

```bash
$ rails s
```

# Zadanie1 and Zadanie2 (PHP)

This is a very simple solution to the problem without using redundant libraries. I used only CSS framework called Bulma.

Only the PHP interpreter and a web server are required to run.



