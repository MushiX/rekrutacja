<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<title>Zadanie 1 | Podsumowanie zamówienia</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
	<link rel="stylesheet" href="base.css"/>
</head>
<body>
	<nav class="navbar is-info">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <h1>MAGAZYN</h1>
      </a>
      <div class="navbar-burger burger" data-target="navigation">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div id="navigation" class="navbar-menu">
      <a class="navbar-item" href="index.php">Produkty</a>
      <a class="navbar-item is-active" href="order.php">Zamówienie</a>

      <div class="navbar-end">
      	<a href="index.php" class="button">RESET</a>
      </div>
    </div>
  </nav>
  <div class="wrapper has-text-centered">

  	<h2>Podsumowanie Zamówienia</h2><hr/>
  	<?php
  		echo '<p class="is-large">'.$_POST['client'].", ".$_POST['phone'].", ".$_POST['email']."</p><br/>";

  		$sum = 0;
  		echo '<table class="table is-striped"><thead><tr>';
  		echo '<th>Nazwa</th><th>Ilość</th><th>Wartość</th><th>Wartość końcowa</th></tr></thead>';
  		echo '<tbody>';
  		for($i=0; !empty($_POST['name'][$i]); $i++){
  			$name = $_POST['name'][$i];
  			$count = $_POST['count'][$i];
  			$price = $_POST['price'][$i];
  			$valueJS = $_POST['value'][$i];
  			$value = $_POST['price'][$i] * $_POST['count'][$i];
  			$sum += $value;
echo<<<END
  			<tr>
  				<td>$name</td><td>$count</td><td>$price</td><td>$valueJS zł(JS) / $value zł(PHP)</td>
  			</tr>
END;
  		}
  		echo '</tbody></table><br/><p class="sum">Suma: '.$sum.'zł'.'</p>';
  	?>

  </div>
  <footer class="footer">
  	<p><a href="http://mushix.eu/cv" target="_blank">MushiX</a> &copy; wszelkie prawa zastrzeżone</p>
    <p id="clock"></p>
  </footer>

  <script src="base.js"></script>
  <script src="clock.js"></script>
</body>
</html>