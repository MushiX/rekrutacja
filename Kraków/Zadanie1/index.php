<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<title>Zadanie 1</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
	<link rel="stylesheet" href="base.css"/>
</head>
<body>
	<nav class="navbar is-info">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <h1>MAGAZYN</h1>
      </a>
      <div class="navbar-burger burger" data-target="navigation">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div id="navigation" class="navbar-menu">
      <a class="navbar-item is-active" href="index.php">Produkty</a>
      <a class="navbar-item" href="order.php">Zamówienie</a>

      <div class="navbar-end">
      	<a href="index.php" class="button">RESET</a>
      </div>
    </div>
  </nav>
  <div class="wrapper has-text-centered">
  	<form action="order.php" method="post">
  		<div class="columns form-user">
		  	<div class="column is-4"><label>Imię</label><input type="text" class="input is-3" name="client" required/></div>
		  	<div class="column is-4"><label>Nr Telefonu</label><input type="number" class="input is-3" name="phone" required/></div>
		  	<div class="column is-4"><label>Email</label><input type="email" class="input is-3" name="email" required/></div>
			</div>

		  <!--<div class="order-item">
		  	<input type="text" class="input" placeholder="Nazwa" name="name[]"/>
		  	<input type="number" min="1" class="input" placeholder="Ilość" name="count[]" onblur="calc()"/>
		  	<input type="number" step="0.01" min="0" class="input" placeholder="Wartość" name="price[]" onblur="calc()"/>
		  	<input type="number" class="input" placeholder="Wartość końcowa" name="value[]" readonly/>
		  </div>-->

		  <input type="submit" value="Zamów" class="button is-info"/>
		</form>

		<button onclick="addItem()" class="button is-primary">Dodaj przedmiot</button>

  </div>
  <footer class="footer">
  	<p><a href="http://mushix.eu/cv" target="_blank">MushiX</a> &copy; wszelkie prawa zastrzeżone</p>
    <p id="clock"></p>
  </footer>

  <script src="base.js"></script>
  <script src="clock.js"></script>
</body>
</html>

<script>
	var i = 0;

	function calc(id){
		let item = document.getElementsByClassName('order-item')[id];
		let count = item.getElementsByTagName('input')[1].value;
		let price = item.getElementsByTagName('input')[2].value;
		let value = item.getElementsByTagName('input')[3];

		if(count!=0 && price!=0){
			value.value = (price*count).toFixed(2);
		} else {
			value.value = 0;
		}
	}

	function addItem(){
		let newDiv = document.createElement("div");
		let inputName = document.createElement("input");
		let inputCount = document.createElement("input");
		let inputPrice = document.createElement("input");
		let inputValue = document.createElement("input");

		inputName.className="input"; inputCount.className="input"; inputPrice.className="input"; inputValue.className="input";
		inputName.type="text"; inputCount.type="number"; inputPrice.type="number"; inputValue.type="number";
		inputName.placeholder="Nazwa"; inputCount.placeholder="Ilość"; inputPrice.placeholder="Wartość"; inputValue.placeholder="Wartość końcowa";
		inputName.name="name[]"; inputCount.name="count[]"; inputPrice.name="price[]"; inputValue.name="value[]";
		inputName.style.margin="2.25px"; inputCount.style.margin="2.25px"; inputPrice.style.margin="2.25px"; inputValue.style.margin="2.25px";
		inputCount.setAttribute("min","1"); inputPrice.setAttribute("min","0"); inputPrice.setAttribute("step","0.01"); inputValue.setAttribute("readonly","");
		inputName.setAttribute("required",""); inputCount.setAttribute("required",""); inputPrice.setAttribute("required",""); inputValue.setAttribute("required","");
		inputCount.setAttribute("onblur","calc("+i+")"); inputPrice.setAttribute("onblur","calc("+i+")");
		newDiv.className="order-item";

		newDiv.appendChild(inputName);
		newDiv.appendChild(inputCount);
		newDiv.appendChild(inputPrice);
		newDiv.appendChild(inputValue);

		let parent = document.getElementsByTagName('form')[0];
		let prevChild = document.getElementsByClassName('button is-info')[0];
		parent.insertBefore(newDiv,prevChild);

		++i;
	}

	addItem();
</script>